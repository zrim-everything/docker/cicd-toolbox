ARG DKR_IMG_BASE_URI
ARG DKR_IMG_BASE_VERSION
FROM $DKR_IMG_BASE_URI:$DKR_IMG_BASE_VERSION
ARG VERSION
LABEL eu.zrim-everything.project.version=$VERSION

# update version
RUN echo $VERSION > $ZE_TOOLBOX_ROOT_PATH/toolbox-version \
    && cd $ZE_TOOLBOX_NODE_PATH \
    && npm version --allow-same-version --no-git-tag-version $VERSION
