const _ = require('lodash'),
  semverUtils = require('semver');

/**
 * Utility to generate the version
 */
class VersionGenerator {

  constructor() {

  }

  /**
   * @typedef {Object} VersionGenerator.generateVersion~Options
   * @property {GitLog~Commit[]} commits The git commits
   * @property {string} branchName The current branch name
   * @property {string} latestVersion The latest version
   */
  /**
   * Generate the version
   * @param {VersionGenerator.generateVersion~Options} options The options
   * @return {string} The version
   */
  generateVersion(options) {
    // TODO Validate the options

    if (options.branchName === 'master') {
      return this._generateMasterVersion(options);
    } else {
      return this._generateBranchVersion(options);
    }
  }

  /**
   * Generate the version for a branch
   * @param {VersionGenerator.generateVersion~Options} options The options
   * @return {string} The version
   */
  _generateBranchVersion(options) {
    const additionalId = process.env.CI_JOB_ID || '0';
    let version = semverUtils.coerce(options.latestVersion) + '-' + _.kebabCase(`${options.branchName}-build`);

    const idNumber = parseInt(additionalId);
    if (_.isNaN(idNumber)) {
      version += `-${idNumber}`;
    } else {
      version += `.${idNumber}`;
    }

    return version;
  }

  /**
   * Generate the version for the master
   * @param {VersionGenerator.generateVersion~Options} options The options
   * @return {string} The version
   */
  _generateMasterVersion(options) {
    const commits = options.commits;

    if (!commits[0].merge) {
      // The latest commit is not a merge so use branch system
      return this._generateBranchVersion(options);
    }

    let previousVersion = options.latestVersion || '0.0.0';

    // Lookup for the latest commit with a tag
    const commitIndexWithTagVersion = this._indexOfCommitWithSemver(options.commits);

    if (commitIndexWithTagVersion >= 0) {
      // Found a commit with a tag version
      const versions = [];
      _.each(commits[commitIndexWithTagVersion].tags, tag => {
        const version = semverUtils.valid(tag);
        if (version) {
          versions.push(version);
        }
      });

      versions.sort((a, b) => semverUtils.lt(a, b) ? 1 : -1);
      previousVersion = semverUtils.gt(options.latestVersion, versions[0]) ? options.latestVersion : versions[0];
    }

    if (commitIndexWithTagVersion === 0) {
      // Use the current version of the branch : No changes
      return previousVersion;
    }

    // Commit with the changes to know how many version we bump
    const commitsWithChanges = _.slice(commits, 0, commitIndexWithTagVersion);

    const commitGroups = this._groupCommitByMerge(commitsWithChanges);

    // For each group we should have a bump, in case we have multiple merges and ci launch
    const bumps = _.filter(_.map(commitGroups, commits => {
      return this._determinateBumpForCommitGroup(commits);
    }), _.isString);

    // An undefined bump is possible, meaning no bump
    let version = previousVersion;

    if (bumps.length === 0) {
      // TODO Which default bump to use
      version = semverUtils.inc(version, 'minor');
    }

    _.each(bumps, type => {
      version = semverUtils.inc(version, type);
    });

    return version;
  }

  /**
   * Search the latest commit with a tag (semver)
   * @param {GitLog~Commit[]} commits The commites
   * @return {number} The index if found otherwise -1
   */
  _indexOfCommitWithSemver(commits) {
    const index = _.findIndex(commits, commit => {
      let hasSemver = false;
      if (commit.tags && commit.tags.length === 0) {
        return false;
      }

      _.each(commit.tags, tag => {
        const version = semverUtils.valid(tag);
        if (version) {
          hasSemver = true;
          return false;
        }
      });

      return hasSemver;
    });

    return index;
  }

  /**
   * Group commit by merge
   * @param {GitLog~Commit[]} commits The commits
   * @return {Array<GitLog~Commit[]>} A group a commits
   */
  _groupCommitByMerge(commits) {
    const groups = [];
    let currentGroup = [];

    // This is the subject merge subject from gitlab
    const regexpSubjectMergeBranch = /Merge branch '[^']+' into 'master'/i;

    _.eachRight(commits, commit => {
      currentGroup.push(commit);

      if (commit.merge && (commit.subject || '').match(regexpSubjectMergeBranch)) {
        // This is a merge commit
        currentGroup = _.reverse(currentGroup);
        groups.push(currentGroup);
        currentGroup = [];
      }
    });

    if (currentGroup.length > 0) {
      currentGroup = _.reverse(currentGroup);
      groups.push(currentGroup);
    }

    return groups;
  }

  /**
   * Determinate the bump for this commit group
   * @param {GitLog~Commit[]} commitGroup The group of commit
   * @return {string|undefined} The bump found
   */
  _determinateBumpForCommitGroup(commitGroup) {
    // In the future could use new detection
    return this._determinateBumpForCommitGroup_mergeWithBranchName(commitGroup);
  }

  /**
   * Determinate the bump type
   * @param {GitLog~Commit[]} commits The group of commit
   * @return {string|undefined} The bump found
   */
  _determinateBumpForCommitGroup_mergeWithBranchName(commits) {
    let bumpType = undefined;

    const regexpSubjectMergeBranch = /Merge branch '([^']+)' into 'master'/i;

    _.each(commits, commit => {
      let matches = (commit.subject || '').match(regexpSubjectMergeBranch);
      if (matches) {
        const branchName = matches[1];

        if (_.startsWith(branchName, 'revert-')) {
          bumpType = 'minor';
          return false;
        }

        const branchType = _.split(branchName, '/')[0].toLowerCase().trim();

        switch (branchType) {
          case 'major':
          case 'breaking':
          case 'breakingchange':
          case 'breakingchanges':
            bumpType = 'major';
            break;
          case 'feature':
          case 'features':
          case 'revert':
          case 'minor':
            bumpType = 'minor';
            break;
          case 'patch':
          case 'bug':
          case 'bugfix':
          case 'bugfixes':
            bumpType = 'patch';
            break;
          default:
            break;
        }

        if (bumpType) {
          return false;
        }
      }
    });

    return bumpType;
  }
}

exports.VersionGenerator = VersionGenerator;
