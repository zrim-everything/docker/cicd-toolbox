#!/usr/bin/env node

const git = require('../git'),
  _ = require('lodash'),
  semverUtils = require('semver'),
  ArgumentParser = require('argparse').ArgumentParser;

// Set by default the exit code to 1
process.exitCode = 1;

const VERSION = require('./../../package.json').version;

const rootParser = new ArgumentParser({
  version: VERSION,
  addHelp: true,
  description: 'Generate version'
});

rootParser.addArgument(
  ['-d', '--project-dir'],
  {
    action: 'store',
    help: 'Set the project directory',
    metavar: 'path',
    dest: 'projectDirPath'
  }
);

const userArguments = rootParser.parseArgs();

const workflowContext = {
  services: {
    git: {
      tag: new git.GitTag(),
      branch: new git.GitBranch(),
      log: new git.GitLog()
    },
    versionGenerator: new (require('../version-generator').VersionGenerator)()
  },
  projectDirPath: userArguments.projectDirPath || undefined
};


async function resolveLatestVersion() {
  const {tags: rawTags} = await workflowContext.services.git.tag.listBranchTags({
    gitOptions: {
      projectDirPath: workflowContext.projectDirPath
    }
  });

  const tagVersions = _.filter(_.map(rawTags, semverUtils.valid), _.isString);
  tagVersions.sort((a, b) => semverUtils.lt(a, b) ? 1 : -1);

  return tagVersions[0];
}

async function main() {
  workflowContext.latestVersion = await resolveLatestVersion() || '0.0.0';
  workflowContext.currentBranchName = process.env.CI_COMMIT_REF_NAME || (await workflowContext.services.git.branch.currentBranchName({
    gitOptions: {
      projectDirPath: workflowContext.projectDirPath
    }
  })).branchName;
  workflowContext.commits = (await workflowContext.services.git.log.fetchCommits({
    gitOptions: {
      projectDirPath: workflowContext.projectDirPath
    }
  })).commits;

  workflowContext.generatedVersion = workflowContext.services.versionGenerator.generateVersion({
    commits: workflowContext.commits,
    branchName: workflowContext.currentBranchName,
    latestVersion: workflowContext.latestVersion
  });
}

main()
  .then(() => {
    process.exitCode = 0;
    process.stdout.write(`${workflowContext.generatedVersion}\n`);
  })
  .catch(error => {
    process.stderr.write(`Error: ${error.message}\n${error.stack}\n`);
    process.exit(_.isNumber(error.exitCode) ? error.exitCode : 1);
  });

