#!/usr/bin/env node

const git = require('../git'),
  _ = require('lodash'),
  semverUtils = require('semver'),
  ArgumentParser = require('argparse').ArgumentParser,
  Gitlab = require('gitlab/dist/es5').default;

// Set by default the exit code to 1
process.exitCode = 1;

const VERSION = require('./../../package.json').version;

const rootParser = new ArgumentParser({
  version: VERSION,
  addHelp: true,
  description: 'Tag version'
});

rootParser.addArgument(
  ['-r', '--release'],
  {
    action: 'store',
    help: 'Set the release version (semver)',
    metavar: 'Release version',
    dest: 'releaseVersion',
    required: true
  }
);

rootParser.addArgument(
  ['-p', '--prefix'],
  {
    action: 'store',
    help: 'Set the prefix to add to the tag',
    metavar: 'Tag prefix',
    dest: 'tagPrefix',
    defaultValue: 'v'
  }
);

rootParser.addArgument(
  ['-m', '--message'],
  {
    action: 'store',
    help: 'Set the tag message',
    metavar: 'Tag message',
    dest: 'tagMessage',
    defaultValue: 'Auto Tagging'
  }
);

rootParser.addArgument(
  ['-M', '--release-message'],
  {
    action: 'store',
    help: 'Set release message',
    metavar: 'Release message',
    dest: 'releaseMessage'
  }
);

rootParser.addArgument(
  ['--project-id'],
  {
    action: 'store',
    help: 'Set the project id',
    metavar: 'Project ID',
    dest: 'projectId'
  }
);

rootParser.addArgument(
  ['--allow-dirty-version'],
  {
    action: 'storeTrue',
    help: 'Allow tagging a dirty version not like major.minor.patch',
    dest: 'allowDirtyVersion',
    defaultValue: false
  }
);

rootParser.addArgument(
  ['-d', '--project-dir'],
  {
    action: 'store',
    help: 'Set the project directory',
    metavar: 'path',
    dest: 'projectDirPath'
  }
);

const userArguments = rootParser.parseArgs();

const gitlabToken = process.env.ZE_GITLAB_TOKEN || process.env.GITLAB_TOKEN;

if (!gitlabToken) {
  process.stderr.write(`No Gitlab Api provided\n`);
  process.exit(5);
}

const workflowContext = {
  services: {
    git: {
      branch: new git.GitBranch()
    },
    gitlab: new Gitlab({
      url: process.env.ZE_GITLAB_URL || process.env.GITLAB_URL || 'https://gitlab.com',
      token: process.env.ZE_GITLAB_TOKEN || process.env.GITLAB_TOKEN
    })
  },
  projectDirPath: userArguments.projectDirPath || undefined
};


function validateInputVersion() {
  workflowContext.inputVersion = semverUtils.valid(userArguments.releaseVersion);
  if (!workflowContext.inputVersion) {
    throw new Error(`Invalid input version '${userArguments.releaseVersion}'`);
  }
}

function resolveProjectId() {
  workflowContext.projectId = userArguments.projectId || process.env.ZE_GITLAB_PROJECT_ID || process.env.CI_PROJECT_ID;

  if (!workflowContext.projectId) {
    throw new Error(`No project identifier provided`);
  }
}

function isDirtyVersion() {
  return !/^\d+\.\d+\.\d+$/i.test(workflowContext.inputVersion);
}

async function rejectIfNotBranchMaster() {
  workflowContext.currentBranchName = (await workflowContext.services.git.branch.currentBranchName({
    gitOptions: {
      projectDirPath: workflowContext.projectDirPath
    }
  })).branchName;

  if (workflowContext.currentBranchName !== 'master') {
    throw new Error(`Process cannot run on the branch '${workflowContext.currentBranchName}'. Require the branch master`);
  }
}

async function fetchVersionTag() {
  let tag = undefined;
  try {
    tag = await workflowContext.services.gitlab.Tags.show(workflowContext.projectId, workflowContext.tagName);
  } catch (error) {
    if (error.statusCode !== 404) {
      throw error; // Propagate the error
    }
  }

  return tag;
}

async function main() {
  await rejectIfNotBranchMaster();
  validateInputVersion();

  // Check the version type
  if (userArguments.allowDirtyVersion !== true && isDirtyVersion()) {
    process.stdout.write(`Dirty version detected. Reject tag`);
    return;
  }

  resolveProjectId();

  workflowContext.tagName = `${userArguments.tagPrefix}${workflowContext.inputVersion}`;

  workflowContext.tag = await fetchVersionTag();
  if (workflowContext.tag) {
    if (process.env.CI_COMMIT_SHA === workflowContext.tag.commit.id) {
      process.stdout.write(`Tag '${workflowContext.tagName}' already defined\n`);
      return;
    }

    throw new Error(`Tag '${workflowContext.tagName}' already exists for the commit id '${workflowContext.tag.commit.id}'`);
  }

  // Create the tag
  const createTagOptions = {
    tag_name: workflowContext.tagName,
    ref: 'master',
    message: userArguments.tagMessage
  };
  if (userArguments.releaseMessage) {
    createTagOptions.release_description = userArguments.releaseMessage;
  }

  workflowContext.tag = await workflowContext.services.gitlab.Tags.create(workflowContext.projectId, createTagOptions);
  workflowContext.tagCreated = true;
}

main()
  .then(() => {
    process.exitCode = 0;
    if (workflowContext.tagCreated === true) {
      process.stdout.write(`Tag '${workflowContext.tag.name}' created with success\n`);
    }
  })
  .catch(error => {
    process.stderr.write(`Error: ${error.message}\n${error.stack}\n`);
    process.exit(_.isNumber(error.exitCode) ? error.exitCode : 1);
  });

