const Joi = require('joi'),
  _ = require('lodash'),
  { spawn } = require('child_process');


/**
 * @typedef {Object} BaseGit~GitBaseOptions
 * @property {string} [projectDirPath] A different project path
 * @property {string} [gitExecPath] A git executable
 */

const gitOptionsSchemaKeys = {
  projectDirPath: Joi.string(),
  gitExecPath: Joi.string()
};

const optionsSchemas = {
  _execute: Joi.object().keys({
    gitOptions: Joi.object().keys(_.assign({}, gitOptionsSchemaKeys)).unknown(),
    commands: Joi.array().items(Joi.string()).required(),
    failOnExitNotZero: Joi.boolean().default(false)
  }).unknown().required()
};

/**
 * Base class to use git
 */
class BaseGit {

  constructor() {

  }

  /**
   * Returns the base keys schema options
   * @return {Object}
   */
  static _getBaseGitOptionsSchema() {
    return gitOptionsSchemaKeys;
  }

  /**
   * Use joi to validate the data by creating a Promise
   * @param {*} options The value to validate
   * @param {Object} schema The schema to validate
   * @return {Promise<any>} The value validated
   */
  _validateOptions(options, schema) {
    return new Promise((resolve, reject) => {
      Joi.validate(options, schema, (error, value) => {
        if (error) {
          const typeError = new TypeError(error.message);
          typeError.cause = error;
          return reject(typeError);
        }

        resolve(value);
      })
    });
  }

  /**
   * @typedef {Object} BaseGit._execute~OnResolve
   * @property {Buffer} stdout The stdout data
   * @property {Buffer} stderr The stderr data
   * @property {Number} exitCode The exit code
   */
  /**
   * @typedef {Object} BaseGit._execute~Options
   * @property {BaseGit~GitBaseOptions} gitOptions The git options
   * @property {string[]} commands The git commands
   * @property {boolean} [failOnExitNotZero] Fail if the exit code is not 0
   */
  /**
   * Execute a git command
   * @param {BaseGit._execute~Options} options The options
   * @return {Promise<BaseGit._execute~OnResolve>}
   */
  _execute(options) {
    return this._validateOptions(options, optionsSchemas._execute)
      .then(validatedOptions => {
        return new Promise((resolve, reject) => {

          const gitExecPath = validatedOptions.gitExecPath || 'git';
          const cmdArgs = [];

          const projectDirPath = _.get(validatedOptions, 'gitOptions.projectDirPath');
          if (projectDirPath) {
            Array.prototype.push.apply(cmdArgs, ['-C', projectDirPath]);
          }

          Array.prototype.push.apply(cmdArgs, validatedOptions.commands);

          const gitExecution = spawn(gitExecPath, cmdArgs);

          const lines = {
            stdout: [],
            stderr: []
          };

          const slots = {
            error: error => {
              reject(error);
            },
            stdout: data => {
              lines.stdout.push(data.toString());
            },
            stderr: data => {
              lines.stderr.push(data.toString());
            },
            close: code => {
              const response = {
                stdout: Buffer.from(_.join(lines.stdout, '')),
                stderr: Buffer.from(_.join(lines.stderr, '')),
                exitCode: code
              };

              if (validatedOptions.failOnExitNotZero === true && response.exitCode !== 0) {
                return reject(new Error(`Command failed with exit code ${_.isNil(response.exitCode) ? 'unknown' : response.exitCode}:\n${response.stderr.toString()}`));
              }

              return resolve(response);
            }
          };

          gitExecution.once('error', slots.error);
          gitExecution.stdout.on('data', slots.stdout);
          gitExecution.stderr.on('data', slots.stderr);
          gitExecution.once('close', slots.close);
        });
      });
  }
}

exports.BaseGit = BaseGit;
