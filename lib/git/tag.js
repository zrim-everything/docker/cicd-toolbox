const BaseGit = require('./base-git').BaseGit,
  Joi = require('joi'),
  _ = require('lodash');

// https://stackoverflow.com/questions/2381665/list-tags-contained-by-a-branch/7698213#7698213

const optionsSchemas = {
  listAllTags: Joi.object().unknown().required(),
  listBranchTags: Joi.object().keys({
    branchName: Joi.string().allow('').allow(null)
  }).unknown().required()
};

/**
 * Handle git tag commands
 */
class GitTag extends BaseGit {

  constructor() {
    super();
  }

  /**
   * Parse a simple tag list
   * @param {string|Buffer|null|undefined} data The output string list
   * @return {string[]} tags The list of tag name
   * @throws {TypeError} In case the input is invalid
   */
  _parseSimpleTagList(data) {
    if (_.isNil(data)) {
      return [];
    } else if (_.isBuffer(data)) {
      data = data.toString();
    } else if (!_.isString(data)) {
      throw new TypeError(`Invalid input type`);
    }

    data = _.replace(data, '\r', '');
    const tags = _.filter(
      _.map(
        _.split(data, '\n'),
        el => el.trim()
      ),
      el => el.length > 0
    );

    return tags;
  }

  /**
   * @typedef {Object} GitTag.listAllTags~OnResolve
   * @property {string[]} tags The tags
   */
  /**
   * @typedef {Object} GitTag.listAllTags~Options
   * @property {BaseGit~GitBaseOptions} [gitOptions] The possible git options
   */
  /**
   * List all tags
   * @param {GitTag.listAllTags~Options} options The options
   * @return {Promise<GitTag.listAllTags~OnResolve>}
   */
  listAllTags(options) {
    return this._validateOptions(options, optionsSchemas.listAllTags)
      .then(() => {
        return this._execute({
          gitOptions: options.gitOptions,
          commands: ['tag', '-l'],
          failOnExitNotZero: true
        })
          .then(result => {
            const tags = this._parseSimpleTagList(result.stdout);

            return Promise.resolve({
              tags: tags
            });
          });
      });
  }

  /**
   * @typedef {Object} GitTag.listBranchTags~OnResolve
   * @property {string[]} tags The tags
   */
  /**
   * @typedef {Object} GitTag.listBranchTags~Options
   * @property {BaseGit~GitBaseOptions} [gitOptions] The possible git options
   * @property {string} [branchName] A possible branch name. By default using HEAD
   */
  /**
   * List all tags
   * @param {GitTag.listBranchTags~Options} options The options
   * @return {Promise<GitTag.listBranchTags~OnResolve>}
   */
  listBranchTags(options) {
    return this._validateOptions(options, optionsSchemas.listBranchTags)
      .then(validatedOptions => {
        const branchName = validatedOptions.branchName || 'HEAD';

        return this._execute({
          gitOptions: options.gitOptions,
          commands: ['tag', '--merged', branchName],
          failOnExitNotZero: true
        })
          .then(result => {
            const tags = this._parseSimpleTagList(result.stdout);
            return Promise.resolve({
              tags: tags
            });
          });
      });
  }
}

exports.GitTag = GitTag;
