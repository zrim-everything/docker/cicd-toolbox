const BaseGit = require('./base-git').BaseGit,
  _ = require('lodash'),
  Joi = require('joi');

const optionsSchemas = {
  currentBranch: Joi.object().unknown().required()
};

/**
 * Handle git branch commands
 */
class GitBranch extends BaseGit {

  constructor() {
    super();
  }

  /**
   * @typedef {Object} GitBranch.currentBranchName~OnResolve
   * @property {string} branchName The current branch name
   */
  /**
   * @typedef {Object} GitBranch.currentBranchName~Options
   * @property {BaseGit~GitBaseOptions} [gitOptions] The possible git options
   */
  /**
   * Returns the current branch name
   * @param {GitBranch.currentBranchName~Options} options The options
   * @return {Promise<GitBranch.currentBranchName~OnResolve>}
   */
  currentBranchName(options) {
    return this._validateOptions(options, optionsSchemas.currentBranch)
      .then(() => {
        // Due to change in gitlab https://docs.gitlab.com/ee/ci/pipelines.html#persistent-pipeline-refs
        // We are going to force to use the environment CI_COMMIT_REF_NAME https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

        // Possible command:
        //    rev-parse --abbrev-ref HEAD
        //    git name-rev b1f0ddce1f3f53dac9705c252cace7ee3e6493bb   > b1f0ddce1f3f53dac9705c252cace7ee3e6493bb name
        //    git name-rev HEAD   > HEAD name
        //    git log --decorate=full -n 1 HEAD And parse log

        if (process.env.CI_COMMIT_REF_NAME) {
          return Promise.resolve({
            branchName: process.env.CI_COMMIT_REF_NAME
          });
        }

        return this._execute({
          gitOptions: options.gitOptions,
          commands: ['name-rev', '--name-only','HEAD'],
          failOnExitNotZero: true
        })
          .then(result => {
            const rawName = result.stdout.toString().trim();

            let branchName = undefined;
            if (_.startsWith(rawName, 'remotes/origin/')) {
              branchName = rawName.substr('remotes/origin/'.length);
            } else if (_.startsWith(rawName, 'tags/')) {
              // Do not know in some case have ^0
              branchName = _.trimEnd(`tag-${rawName.substr('tags/'.length)}`, '^0');
            } else {
              branchName = rawName;
            }

            return Promise.resolve({
              branchName: branchName
            });
          });
      });
  }
}

exports.GitBranch = GitBranch;
