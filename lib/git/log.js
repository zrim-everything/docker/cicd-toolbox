const BaseGit = require('./base-git').BaseGit,
  _ = require('lodash'),
  Joi = require('joi');

const optionsSchemas = {
  fetchCommits: Joi.object().keys({
    branchName: Joi.string(),
    maxCommits: Joi.number().positive().default(200)
  }).unknown().required()
};

/**
 * @typedef {Object} GitLog~Commit~Author
 * @property {string} name The author name
 * @property {string} [email] The author email
 */
/**
 * @typedef {Object} GitLog~Commit~Merge
 * @property {string} from The short commit id where the merge come from
 * @property {string} to The short commit id where the merge go to
 */
/**
 * @typedef {Object} GitLog~Commit
 * @property {string} raw The raw commit string
 * @property {string[]} rawLines The raw commit lines
 * @property {string} id The commit id
 * @property {string} [branchName] The branch name if specified
 * @property {string} subject The subject
 * @property {string} [message] The message
 * @property {string[]} tags The tags
 * @property {GitLog~Commit~Author} [author] The author
 * @property {GitLog~Commit~Merge} [merge] The merge information
 */

/**
 * Handle git log commands
 */
class GitLog extends BaseGit {

  constructor() {
    super();
  }

  /**
   * @typedef {Object} GitLog.parseLog~Options
   * @property {string} log The raw git log
   */
  /**
   * Parse the git log
   * @param {GitLog.parseLog~Options} options The options
   */
  parseLog(options) {
    const rawLog = options.log;
    const lines = _.split(rawLog, '\n');

    const commitLineRegexp = /^commit ([a-zA-Z0-9]{40})(?:\s+\(([^)]+)\))?/;

    // Check the first line
    if (!lines[0].match(commitLineRegexp)) {
      throw new Error(`Invalid log. Must start with a commit line`);
    }

    const commits = [];

    const handleCommitBlock = lines => {
      const endHeaderIndex = _.findIndex(lines, _.isEmpty);

      const commit = {
        raw: _.join(lines, '\n'),
        rawLines: lines,
        tags: []
      };

      const headerLines = endHeaderIndex == -1 ? lines : _.slice(lines, 0, endHeaderIndex);
      const bodyLines = endHeaderIndex == -1 ? [] : _.slice(lines, endHeaderIndex + 1);

      _.each(headerLines, line => {
        let matches = line.match(commitLineRegexp);
        if (matches) {
          // New group
          commit.id = matches[1];

          if (matches[2]) {
            // Split by ,
            const commitRefParts = _.map(matches[2].split(','), el => el.trim());

            _.each(commitRefParts, part => {
              let matches = part.match(/^tag:\s+refs\/tags\/(.+)$/i);
              if (matches) {
                // Tag
                commit.tags.push(matches[1].trim());
              }

              matches = part.match(/^HEAD\s+->\s+refs\/heads\/(.+)$/i);
              if (matches) {
                commit.branchName = matches[1];
              }
            });

          }
          return;
        }

        matches = line.match(/^Merge:\s+([a-zA-Z0-9]{7})\s+([a-zA-Z0-9]{7})$/)
        if (matches) {
          // Info about merge
          commit.merge = {
            from: matches[1],
            to: matches[2]
          };
          return;
        }

        matches = line.match(/^Author:\s+(.+)(?:\s+<([^>]+)>)$/);
        if (matches) {
          // Info about author
          commit.author = {
            name: matches[1].trim(),
            email: (matches[2] || '').trim()
          };
          return;
        }

        matches = line.match(/^Date:\s+(.+)$/);
        if (matches) {
          // Info about date
          commit.createdAt = new Date(Date.parse(matches[1]));
          return;
        }
      });

      // Parse the body
      commit.subject = _.trimStart(bodyLines[0]);
      commit.message = _.join(_.map(_.slice(bodyLines, 2), _.trimStart), '\n');

      commits.push(commit);
    };

    let startBlockIndex = 0;
    while (startBlockIndex >= 0) {
      const nextBlockIndex = _.findIndex(lines, v => !!v.match(commitLineRegexp), startBlockIndex + 1);

      if (nextBlockIndex < 0) {
        // End of the block
        handleCommitBlock(_.slice(lines, startBlockIndex));
        startBlockIndex = -1;
      } else {
        // New block
        handleCommitBlock(_.slice(lines, startBlockIndex, nextBlockIndex));
        startBlockIndex = nextBlockIndex;
      }
    }

    return commits;
  }

  /**
   * @typedef {Object} GitLog.fetchCommits~OnResolve
   * @property {GitLog~Commit[]} commits The commits
   */
  /**
   * @typedef {Object} GitLog.fetchCommits~Options
   * @property {BaseGit~GitBaseOptions} [gitOptions] The possible git options
   * @property {string} [branchName] Use this branch name instead of HEAD
   * @property {Number} [maxCommits] The maximum number of commits
   */
  /**
   * Fetch the commits
   * @param {GitLog.fetchCommits~Options} options The options
   * @return {Promise<GitLog.fetchCommits~OnResolve>}
   */
  fetchCommits(options) {
    return this._validateOptions(options, optionsSchemas.fetchCommits)
      .then(validatedOptions => {
        const branchName = validatedOptions.branchName || 'HEAD';

        return this._execute({
          gitOptions: options.gitOptions,
          commands: ['log', '--decorate=full', `--max-count=${validatedOptions.maxCommits}`, branchName],
          failOnExitNotZero: true
        })
          .then(result => {
            const commits = this.parseLog({
              log: result.stdout.toString().trim()
            });
            return Promise.resolve({
              commits: commits
            });
          });
      });
  }
}

exports.GitLog = GitLog;
