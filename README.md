# Zim-Everthing - CI/CD Toolbox

## Introduction

Special docker image meant to be used in gitlab-ci.

## Environment variables

- `ZE_TOOLBOX_ROOT_PATH` Contains the root path to the toolbox

## Commands

### ToolBox version

```bash
toolbox-version
```

Returns the toolbox version

### Get project version

```bash
get-project-version
```

Returns the project version available.

This project will try to look in the file where the cicd-toolbox save the version.
This use the environment variable `ZE_GENERATED_VERSION_FILENAME` to know where the file is
located. If not provided, using the file name `VERSION`.

### Npm Set version

```bash
npm-set-version
```

Set the package.json version.
Use the command `get-project-version` if the version is not provided.

Options:
- --version <version> : The version to use
- --with-git : Use the git feature of npm (Default not enabled)

### Npm Login

```bash
npm-login
```

Set the token for npm to login to the repository.

Options:
- --token <token> : Override the environment variable *ZE_NPM_AUTH_TOKEN*
- --registry <domain> : Override the domain registry environment variable *NPM_REGISTRY_DOMAIN_NAME*

### Npm publish

```bash
npm-publish
```

Publish your project to the repository

Options
- --snapshot : Indicate the publication is a snapshot
- --registry <uri> : Override the registry uri from the environment variable *NPM_REGISTRY_URL*

In case `DISABLE_NPM_PUBLISH` is defined, the publish will be ignore.

In case `ENABLE_NPM_PUBLISH_SNAPSHOT` is not defined, snapshot will not be published.

### Aws

Enable the provide wasaby: `enable-wasabisys`

To use `ze-aws` or directly `aws`.

The `ze-aws` allow to override the environment variable for s3:
- ZE_AWS_ACCESS_KEY_ID
- ZE_AWS_SECRET_ACCESS_KEY
- ZE_AWS_SESSION_TOKEN


### Launch Script Bundle

```bash
launch-script-bundle
```

Launch a bundle of user defined script.

Options:
- --location <location> : Define the root location of the bundle directory
- --bundle <name> : The bundle name to use (directory name)
- --recursive : Using recursive mode (multiple sub task)
- --ignore-not-found : Ignore a bundle/script not found

#### No Recursive mode

In no recursive mode, you must provide a script `main.sh` in the directory in executable mode.
This script will be call.

#### Define bundle

<ol>
  <li>Create a directory under `your-root-location`</li>
  <li>
    Create a bash script named `main.sh` in your new directory. 
    <i>It does not required to be executable.</i>
  </li>
  <li>
    Create if necessary pre actions to execute before you main script in <b>your-bundle-dir/pre</b>.
    Each script must have the extension <b>sh</b>.
    The execution order will be define by the file name
  </li>
  <li>
    Create if necessary post actions to execute after you main script in <b>your-bundle-dir/post</b>.
    Each script must have the extension <b>sh</b>.
    The execution order will be define by the file name
  </li>
  <li>
    Create if necessary clean-up actions to execute at the end whatever if pre/main/post action succeed <b>your-bundle-dir/clean-up</b>.
    Each script must have the extension <b>sh</b>.
    The execution order will be define by the file name
  </li>
</ol>

About workflow:
- The main script will only be executed if all pre action succeed.
- The post actions will only be executed if the main action has been launch
- The clean-up actions are always executed
- If a pre-action failed, the following ones will not be executed
- If a post/clean-up action failed, the following ones will be executed

##### Actions

Actions:
- pre
- post
- clean-up

The script will have access to the environment variables:
- ZE_BUNDLE_NAME : The bundle name (directory name of the bundle suite)
- ZE_REPORT_DIR_PATH : The absolute directory path where to save reports
- ZE_ACTION_DIR_PATH : The absolute directory path where all actions 
  with the current type are stored
- ZE_ACTION_FILE_PATH : The absolute file name path of the script
- ZE_ACTION_TYPE : The action currently running

The actions script must return 0 if success otherwise it will considered as error.

The post action will have access to the special environment variables:
- ZE_BUNDLE_MAIN_SCRIPT_EXIT_CODE : Which contains the exit code returned by the main script

##### Main

The script will have access to the environment variables:
- ZE_BUNDLE_NAME : The test name (directory name of the test suite)
- ZE_REPORT_DIR_PATH : The absolute directory path where to save reports

#### Recursive mode

The recursive mode allow to have multiple directory in one bundle. Each directory is executed
like a bundle in no recursive mode.

The execution order use the file name order.

#### Tips

To order your script you can use the kind of standard:
<number in 4 digits>-my-name.sh

Example:
- 0010-test.sh
- 0020-test-2.sh

Using a 10 increment number help to place a new script between 2 without changing all names like:
- 0010-test.sh
- 0015-my-new-file.sh
- 0020-test-2.sh

#### Example

Project structure
- root-project
  - scripts (You bundle root location)
    - test (Root test directory)
      - unit (Bundle for unit test)
      - integration (Bundle for integration test)
    - pre-deployment (Pre deployment bundle)
    
Launch your bundles
```bash
launch-script-bundle --location root-project/scripts/test --ignore-not-found --bundle unit
```
This command launch a bundle name unit. So the main file may be in it.
      


