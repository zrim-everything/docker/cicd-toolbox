#!/usr/bin/env bash

chmod -R a+x $CI_PROJECT_DIR/scripts

for fname in $(find $CI_PROJECT_DIR/scripts -name '*.sh' -type f); do
    chmod a+x $fname;
    ln -s $fname /usr/bin/$(basename "$fname" .sh);
done
