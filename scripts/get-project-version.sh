#!/usr/bin/env bash

#
# Returns the project version
#

if [ -n "$ZE_GENERATED_VERSION_FILENAME" ]; then
  if [ -f "$ZE_GENERATED_VERSION_FILENAME" ]; then
    cat $ZE_GENERATED_VERSION_FILENAME
    exit 0
  fi;
fi;

# Base version file fichier from cicd-toolbox pipeline
if [ -f "VERSION" ]; then
  cat VERSION
  exit 0
fi;

echo "Version file not available" >&2
exit 1
