#!/usr/bin/env bash

if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi

node $ZE_TOOLBOX_NODE_PATH/lib/bin/tag-version.js "$@"
exit $?
