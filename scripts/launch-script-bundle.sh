#!/usr/bin/env bash

#
# Script used to launch bundle scripts
#

if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi

COMMAND_LINE_OPTIONS_HELP='
Command line options:
    -l/--location <location>    The root location where to look for bundle
    -b/--bundle <bundle-name>   The bundle to launch
    -r/--recursive              Use recursive mode in the bundle
    -i/--ignore-not-found       Ignore error if the bundle is not found
    -h/--help                   Print this help menu
'

print_help() {
    echo $COMMAND_LINE_OPTIONS_HELP
}

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=l:b:rih
LONGOPTIONS=recursive,location:,bundle:,ignore-not-found,help

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    exit 2
fi

eval set -- "$PARSED"

while true; do
    case "$1" in
        -b|--bundle)
            bundle_name=$2
            shift 2
            ;;
        -l|--location)
            bundle_root_location=$2
            shift 2
            ;;
        -h|--help)
            print_help
            exit 0
            ;;
        -r|--recursive)
            enable_recursive_mode="true"
            shift
            ;;
        -i|--ignore-not-found)
            ignore_bundle_not_found="true"
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ -z "$bundle_name" ]; then
  print_help
  exit 2
fi;

if [ -z "$bundle_root_location" ]; then
  print_help
  exit 2
fi;

# ======================================================
# ======================================================

# Save the current path variable
original_path=$PATH
export PATH="$PATH:$CI_PROJECT_DIR/.cicd/bin"

# ======================================================
# ======================================================

do_exit() {
  export PATH=$original_path
  exit $1
}

exit_with_missing_bundle() {
  if [ -z "$ignore_bundle_not_found" ]; then
    do_exit 1;
  else
    do_exit 0;
  fi;
}

# Launch a script
launch_script() {
  script_path=$1
  #echo $script_path

  bash $script_path
  script_exit_code=$?
  #if [ -x $script_path ]; then
  #  echo "executable file?"
  #  $script_path
  #  script_exit_code=$?
  #else
  #  echo "running script"
  # bash $script_path
  #  script_exit_code=$?
  #fi;

  return $script_exit_code
}

launch_action() {
  bundle_dir_path=$1
  action_type=$2
  ignore_error=$3
  action_dir_path=$bundle_dir_path/$action_type
  bundle_name=$ZE_BUNDLE_NAME

  if [ -d $action_dir_path ]; then
    # Now we check for file to launch
    for f in $(find $action_dir_path/ -type f -name '*.sh' -print | sort); do
        base_filename=$(basename $f)
        relative_file_path="$action_type/$base_filename"
        echo "[Bundle:$bundle_name][Action:$action_type][File:$relative_file_path] Execute script"

        # Export environment variables
        export ZE_ACTION_DIR_PATH=$action_dir_path
        export ZE_ACTION_FILE_PATH=$f
        export ZE_ACTION_TYPE=$action_type

        launch_script $f
        script_exit_code=$?

        # Unset environment variables
        unset ZE_ACTION_DIR_PATH
        unset ZE_ACTION_FILE_PATH
        unset ZE_ACTION_TYPE

        if [ $script_exit_code -ne 0 ]; then
            echo "[Bundle:$bundle_name][Action:$action_type][File:$relative_file_path] exit with code=$script_exit_code" >&2
            if [[ $ignore_error == 'true' ]]; then
              echo "[Bundle:$bundle_name][Action:$action_type][File:$relative_file_path] Policy=ignoreError"
            else
              return 1
            fi;
        fi
    done
  fi;
}

# Return 0 if exists, otherwise not found
does_bundle_have_main() {
  file_path=$1/main.sh

  if [ -f $file_path ]; then
    return 0;
  else
    return 1;
  fi;
}

# Launch a full bundle
launch_bundle() {
  bundle_dir_path=$1
  bundle_name=$(basename $bundle_dir_path)
  main_bundle_script=$bundle_dir_path/main.sh

  if [ -f $main_bundle_script ]; then
    # Export environment variable
    export ZE_BUNDLE_NAME=$bundle_name
    export ZE_REPORT_DIR_PATH="$CI_PROJECT_DIR/reports"

    echo ""
    echo "____________________________________________"
    echo "| Main bundle: $bundle_name"
    echo "| Location: $main_bundle_script"
    echo "--------------------------------------------"

    echo "[Bundle:$bundle_name] Find main script file: $main_bundle_script"
    echo "[Bundle:$bundle_name] Run pre actions"
    launch_action $bundle_dir_path "pre"
    pre_action_exit_code=$?
    if [ $pre_action_exit_code -eq 0 ]; then
      # Launch main script
      echo "[Bundle:$bundle_name] Launching main script"
      launch_script $main_bundle_script
      main_script_exit_code=$?

      if [ $main_script_exit_code -ne 0 ]; then
        echo "[Bundle:$bundle_name] Main script exit with error: $main_script_exit_code" >&2
      fi;

      # Exports informations
      export ZE_BUNDLE_MAIN_SCRIPT_EXIT_CODE=$main_script_exit_code

      echo "[Bundle:$bundle_name] Launch post script"
      launch_action $bundle_dir_path "post" "true" # Ignore exit code error

      # Clean exports
      unset ZE_BUNDLE_MAIN_SCRIPT_EXIT_CODE
    else
      echo "[Bundle:$bundle_name] One or more pre action failed" >&2
    fi;

    echo "Launch clean up script"
    launch_action $bundle_dir_path "clean-up" "true" # Ignore exit code error

    echo "--------------------------------------------"
    echo "| End: $bundle_name"
    echo "____________________________________________"

    # Clea up exports
    unset ZE_BUNDLE_NAME
    unset ZE_REPORT_DIR_PATH

    if [ $pre_action_exit_code -ne 0 ] || [ $main_script_exit_code -ne 0 ]; then
      # Exit due to main failure
      echo "[Bundle:$bundle_name] Exit bundle due to error from action or main script"
      do_exit 1
    fi;
  fi;
}

# ======================================================
# ======================================================

if [ ! -d $bundle_root_location ]; then
  echo "Bundle directory '$bundle_root_location' not found."
  exit_with_missing_bundle
fi;

if [ ! -d "$bundle_root_location/$bundle_name" ]; then
  echo "Cannot find '$bundle_root_location/$bundle_name'"
  exit_with_missing_bundle
fi;

if [ -z "$enable_recursive_mode" ]; then
  does_bundle_have_main "$bundle_root_location/$bundle_name"
  if [ $? -ne 0 ]; then
    echo "Missing the main script for '$bundle_name'"
    exit_with_missing_bundle
  fi;

  # Launch the test
  launch_bundle "$bundle_root_location/$bundle_name"
else
  # Do recusive mode
  root_bundles_dir_path=$bundle_root_location/$bundle_name
  if [ ! -d "$root_bundles_dir_path" ]; then
    exit_with_missing_bundle
  fi;

  for bundle_dir_path in $(ls -d $root_bundles_dir_path/* | sort); do
    launch_bundle $bundle_dir_path
  done
fi;

do_exit 0
