#!/usr/bin/env bash

if [ -n "$ZE_DKR_REGISTRY_URI" ]; then
    echo $ZE_DKR_REGISTRY_URI
    exit 0
fi

echo registry.gitlab.com/${CI_PROJECT_PATH}
