#!/usr/bin/env bash

# Docker build
if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi

COMMAND_LINE_OPTIONS_HELP='
Command line options:
    -f/--file <file>            The docker file
    --image-tag <tag>           The build image tag to use
    -t/--tag <tag>              The docker tag to add
    -a/--arg <arg>              The docker build-arg
    -h/--help                   Print this help menu
'

print_help() {
    echo $COMMAND_LINE_OPTIONS_HELP
}

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=t:a:f:h
LONGOPTIONS=tag:,arg:,file:,image-tag:,help

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    exit 2
fi

eval set -- "$PARSED"

# Set variable
docker_file=Dockerfile
docker_image_name=$(docker-get-registry)
docker_main_image="$docker_image_name:$CI_COMMIT_SHA"
docker_args=""
declare -a docker_tags

while true; do
    case "$1" in
        --image-tag)
            docker_main_image="$docker_image_name:$2"
            shift 2
            ;;
        -t|--tag)
            docker_tags=("${docker_tags[@]}" $2)
            shift 2
            ;;
        -f|--file)
            docker_file=$2
            shift 2
            ;;
        -a|--arg)
            docker_args="$docker_args --build-arg $2"
            shift 2
            ;;
        -h|--help)
            print_help
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

docker_args="$docker_args -t $docker_main_image --file $docker_file"

docker build $docker_args .
exit_code=$?
if [ $exit_code -ne "0" ]; then
    echo "Build failed with code $exit_code";
    exit 1
fi

docker push $docker_main_image
exit_code=$?
if [ $exit_code -ne "0" ]; then
    echo "Push failed with code $exit_code";
    exit 1
fi

# Add new tags
for tag in "${docker_tags[@]}"; do
    docker tag $docker_main_image $docker_image_name:$tag
    exit_code=$?
    if [ $exit_code -ne "0" ]; then
        echo "Tag failed with code $exit_code";
        exit 1
    fi

    docker push $docker_image_name:$tag
    exit_code=$?
    if [ $exit_code -ne "0" ]; then
        echo "Push failed with code $exit_code";
        exit 1
    fi
done

