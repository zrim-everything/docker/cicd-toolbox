#!/usr/bin/env bash

# Docker login
if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi

COMMAND_LINE_OPTIONS_HELP='
Command line options:
    -u/--user <user>            The docker user name
    -p/--password <password>    The docker password
    -d/--domain <domain>        The docker registry domain
    -h/--help                   Print this help menu
'

print_help() {
    echo $COMMAND_LINE_OPTIONS_HELP
}

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=u:p:d:h
LONGOPTIONS=user:,password:,domain:,help

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    exit 2
fi

eval set -- "$PARSED"

# Set variable
docker_user=gitlab-ci-token
docker_password=$CI_BUILD_TOKEN
registry_domain=registry.gitlab.com

while true; do
    case "$1" in
        -u|--user)
            docker_user=$2
            shift 2
            ;;
        -p|--password)
            docker_password=$2
            shift 2
            ;;
        -d|--domain)
            registry_domain=$2
            shift 2
            ;;
        -h|--help)
            print_help
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ -z "$docker_user" ]; then
  echo "Missing docker user" >&2;
  exit 1;
fi;

if [ -z "$docker_password" ]; then
  echo "Missing docker password" >&2;
  exit 1;
fi;

if [ -z "$registry_domain" ]; then
  echo "Missing docker registry domain" >&2;
  exit 1;
fi;

docker login -u "$docker_user" -p "$docker_password" "$registry_domain"
exit $exit_code
