#!/usr/bin/env bash

# Get the registry
if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi

COMMAND_LINE_OPTIONS_HELP='
Command line options:
    -d/--domain                 Show the domain and not the url
    -h/--help                   Print this help menu
'

print_help() {
    echo $COMMAND_LINE_OPTIONS_HELP
}

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=dh
LONGOPTIONS=domain,help

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    exit 2
fi

eval set -- "$PARSED"

# Set variable
default_registry_domain=${ZE_NPM_REGISTRY_DOMAIN_NAME:-www.npmjs.com}
default_registry_url=${ZE_NPM_REGISTRY_URL:-https://${default_registry_domain}/}

while true; do
    case "$1" in
        -d|--domain)
            default_registry_domain=$2
            shift 1
            ;;
        -h|--help)
            print_help
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ -n "$show_only_domain" ]; then
  if [ -n "$ZE_NPM_REGISTRY_DOMAIN_NAME" ]; then
    echo $ZE_NPM_REGISTRY_DOMAIN_NAME;
    exit 0;
  fi;

  echo "$default_registry_domain"
  exit 0
fi;

if [ -n "$ZE_NPM_REGISTRY_URL" ]; then
  echo $ZE_NPM_REGISTRY_URL;
  exit 0;
fi;

echo $default_registry_url
exit 0
