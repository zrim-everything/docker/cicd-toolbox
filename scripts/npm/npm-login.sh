#!/usr/bin/env bash

##
## Script to login to npm
##
if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi


COMMAND_LINE_OPTIONS_HELP='
Command line options:
    -t/--token <token>         Npm Token server. (Override ZE_NPM_AUTH_TOKEN)
    -r/--registry <registry>   Npm registry server. Only the domain name. (Override NPM_REGISTRY_DOMAIN_NAME)
    -h/--help                   Print this help menu
'

print_help() {
    echo $COMMAND_LINE_OPTIONS_HELP
}

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=t:r:h
LONGOPTIONS=token:,registry:,help

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    exit 2
fi

eval set -- "$PARSED"

# Set variable
npm_token=$ZE_NPM_AUTH_TOKEN
npm_registry_domain=${ZE_NPM_REGISTRY_DOMAIN:-$(get-npm-registry -d)}

while true; do
    case "$1" in
        -t|--token)
            npm_token=$2
            shift 2
            ;;
        -r|--registry)
            npm_registry_domain=$2
            shift 2
            ;;
        -h|--help)
            print_help
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ -z "$npm_token" ]; then
  echo Require a npm token
  exit 1
fi;

if [ -z "$npm_registry_domain" ]; then
  echo Require a npm domain name
  exit 1
fi;


echo "Save the token to ~/.npmrc"
echo //$npm_registry_domain/:_authToken=$npm_token >> ~/.npmrc
