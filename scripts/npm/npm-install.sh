#!/usr/bin/env bash

if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi

COMMAND_LINE_OPTIONS_HELP='
Command line options:
    -p/--production             Install in production mode (default mode)
    -a/--all                    Install all module
    -r/--registry <registry>    The npm registry (Override the environment variable NPM_REGISTRY_URL)
    -h/--help                   Print this help menu
'

print_help() {
    echo $COMMAND_LINE_OPTIONS_HELP
}

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=r:pah
LONGOPTIONS=registry:,production,all,help

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    exit 2
fi

eval set -- "$PARSED"

npm_registry=$(get-npm-registry)

while true; do
    case "$1" in
        -p|--production)
            npm_install_production="true"
            shift
            ;;
        -a|--all)
            npm_install_all="true"
            shift
            ;;
        -r|--registry)
            npm_registry=$2
            shift 2
            ;;
        -h|--help)
            print_help
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ -z "$npm_registry" ]; then
  echo Npm registry not provided
  exit 1
fi;

npm_args="--registry=$npm_registry"

if [ -n "$npm_install_production" ]; then
  npm_args=$npm_args" --only=production"
else
  if [ -z "$npm_install_all" ]; then
    npm_args=$npm_args" --only=production"
  fi
fi

npm install $npm_args
exit $?
