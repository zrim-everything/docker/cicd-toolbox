#!/usr/bin/env bash

if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi

COMMAND_LINE_OPTIONS_HELP='
Command line options:
    -s/--snapshot               Publish a snapshot
    -r/--registry <registry>    The npm registry (Override the environment variable NPM_REGISTRY_URL)
    --no-login                  Do not call npm login
    -h/--help                   Print this help menu
'

print_help() {
    echo $COMMAND_LINE_OPTIONS_HELP
}

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=r:sh
LONGOPTIONS=registry:,snapshot,help

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    exit 2
fi

eval set -- "$PARSED"

npm_registry=$(get-npm-registry)

while true; do
    case "$1" in
        -s|--snapshot)
            publish_snapshot="true"
            shift
            ;;
        --no-login)
            no_login="true"
            shift
            ;;
        -r|--registry)
            npm_registry=$2
            shift 2
            ;;
        -h|--help)
            print_help
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ -z "$npm_registry" ]; then
  echo Npm registry not provided
  exit 1
fi;

if [ -n "$ZE_NPM_DISABLE_PUBLISH" ]; then
  echo Publish disabled
  exit 0;
fi;

if [ -z "$no_login" ]; then
  npm-login
  if [[ $? -ne 0 ]]; then
    echo "Failed to login" >&2
    exit 1
  fi;
fi;


if [ -n "$publish_snapshot" ]; then
  # Check if enable publish
  if [ -z "$ZE_NPM_ENABLE_PUBLISH_SNAPSHOT" ]; then
    echo Npm publish Snapshot disabled
    exit 0;
  fi;

  tag_arg="--tag snapshot"
fi;

echo "Package version" $(npm version)
echo "Publish package"
exec npm publish --registry=$npm_registry $tag_arg
