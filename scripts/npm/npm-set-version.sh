#!/usr/bin/env bash

if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi

COMMAND_LINE_OPTIONS_HELP='
Command line options:
    -v/--version <version>      Override the version of the project
    --with-git                  Accept to use git feature of npm
    -h/--help                   Print this help menu
'

print_help() {
    echo $COMMAND_LINE_OPTIONS_HELP
}

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=v:h
LONGOPTIONS=version:,with-git,help

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    exit 2
fi

eval set -- "$PARSED"

project_version=

while true; do
    case "$1" in
        -v|--version)
            project_version=$2
            shift 2
            ;;
        --with-git)
            use_npm_git_feature='true'
            shift
            ;;
        -h|--help)
            print_help
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ -z "$project_version" ]; then
  # Version not provided
  project_version=$(get-project-version)
  if [[ $? -ne 0 ]]; then
    echo "Failed to get project version" >&2
    exit 1
  fi;
fi;

npm_args="--allow-same-version"
if [ -z "$use_npm_git_feature" ]; then
  npm_args=$npm_args" --no-git-tag-version"
fi;

echo "Set the project version to $project_version"
npm version $npm_args $project_version
