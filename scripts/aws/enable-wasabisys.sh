#!/usr/bin/env bash

# Enable the https://wasabi.com/

if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi

COMMAND_LINE_OPTIONS_HELP='
Command line options:
    -r/--region <location>      The region to set as default
    -h/--help                   Print this help menu
'

print_help() {
    echo $COMMAND_LINE_OPTIONS_HELP
}

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=r:h
LONGOPTIONS=region:,help

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    exit 2
fi

eval set -- "$PARSED"

while true; do
    case "$1" in
        -r|--region)
            s3_region=$2
            shift 2
            ;;
        -h|--help)
            print_help
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

if [ -z "$s3_region" ]; then
  if [ -n "$ZE_AWS_S3_REGION" ]; then
    s3_region=$ZE_AWS_S3_REGION
  fi;
fi

if [ -z "$s3_region" ]; then
  s3_region="us-east-1"
fi

# Create the aws directory
mkdir -p ~/.aws
chmod 750 ~/.aws

cat > ~/.aws/config <<EOF
[plugins]
endpoint = awscli_plugin_endpoint

[default]
region = $s3_region
s3 =
  endpoint_url = https://s3.wasabisys.com
  max_concurrent_requests = 100
  max_queue_size = 1000
s3api =
  endpoint_url = https://s3.wasabisys.com

EOF

