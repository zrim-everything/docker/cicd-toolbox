#!/usr/bin/env bash

# This is to enable environment variable

if [ -n "$ZE_AWS_ACCESS_KEY_ID" ]; then
  export AWS_ACCESS_KEY_ID=$ZE_AWS_ACCESS_KEY_ID
fi;

if [ -n "$ZE_AWS_SECRET_ACCESS_KEY" ]; then
  export AWS_SECRET_ACCESS_KEY=$ZE_AWS_SECRET_ACCESS_KEY
fi;

if [ -n "$ZE_AWS_SESSION_TOKEN" ]; then
  export AWS_SESSION_TOKEN=$ZE_AWS_SESSION_TOKEN
fi;

if [[ "$ZE_CICD_ENABLE_VERBOSE_MODE" == "true" ]]; then
    set -x
fi

# Launch the aws command
exec aws "$@"
