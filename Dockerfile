# Allow to override the nodejs version to use
ARG NODEJS_VERSION=14.15.1-buster-slim

FROM node:${NODEJS_VERSION}

ARG VERSION
ARG X_DOCKER_IMG_EXPIRATION=1w

LABEL maintainer="NEGRO, Eric <yogo95@zrim-everything.eu>" \
    eu.zrim-everything.project.name="cicd-toolbox" \
    eu.zrim-everything.project.type="tool" \
    eu.zrim-everything.project.description="Toolbox to build gitlab project using gitlab-ci" \
    eu.zrim-everything.project.git.url="https://gitlab.com/zrim-everything/docker/cicd-toolbox"

ARG HTTP_PROXY
ARG HTTPS_PROXY
ARG NO_PROXY
ARG TYPESCRIPT_VERSION=4

# Ensure to use proxy if needed
RUN if [ -n "${HTTP_PROXY}" ]; then \
        echo "Acquire::http::Proxy \"${HTTP_PROXY}\";" >> /etc/apt/apt.conf.d/proxy.conf; \
    fi; \
    if [ -n "${HTTPS_PROXY}" ]; then \
      echo "Acquire::https::Proxy \"${HTTPS_PROXY}\";" >> /etc/apt/apt.conf.d/proxy.conf; \
    fi;

# Install requirement for the ubiservices-pipeline-cli
RUN apt-get update \
    && apt-get install -y libgssapi-krb5-2 \
    wget curl git build-essential \
    python tar zip jq less \
    # Force to clean up the proxy setting
    && rm -f /etc/apt/apt.conf.d/proxy.conf

RUN npm install npm@latest -g --unsafe-perm=true \
    && npm install -g --production --unsafe-perm=true node-gyp@8 \
    && npm install -g --production --unsafe-perm=true typescript@${TYPESCRIPT_VERSION} \
    && npm install -g --production --unsafe-perm=true ts-node@9.1 \
    && npm cache clean --force

#
# Require:
#   - python 2
#   - git
#   - build tools (c++, make, etc)
#

#RUN rm -rf /var/cache/apk/* && \
#    rm -rf /tmp/* && \
#    apk update && \
#    apk upgrade && \
#    apk --update --update-cache add --no-cache \
#    bash wget curl git lftp tar zip jq bzip2 bind-tools less openssh \
#    libstdc++ libgcc linux-headers make binutils-gold gnupg \
#    openssl-dev \
#    # build-base is a heavy package more than 120 MB
#    build-base \
#    python \
#    #
#    # Clean
#    #
#    && rm -f /var/cache/apk/* \


ENV ZE_TOOLBOX_ROOT_PATH=/usr/local/zrim-everything \
    ZE_TOOLBOX_NODE_PATH=/usr/local/zrim-everything/node-toolbox \
    ZE_TOOLBOX_FEATURES="standard"

RUN mkdir -p $ZE_TOOLBOX_ROOT_PATH && \
  mkdir -p $ZE_TOOLBOX_ROOT_PATH/bin \
  && mkdir -p $ZE_TOOLBOX_NODE_PATH

#
# ======================================
#           Node.js Toolbox
#
COPY package.json package-lock.json $ZE_TOOLBOX_NODE_PATH/
COPY lib/ $ZE_TOOLBOX_NODE_PATH/lib/

RUN cd $ZE_TOOLBOX_NODE_PATH \
    && npm install --production \
    && chmod a+x $ZE_TOOLBOX_NODE_PATH/lib/bin/*.js

#
# ======================================
#

COPY scripts $ZE_TOOLBOX_ROOT_PATH/bin
RUN export ZE_TOOLBOX_BIN_PATH=$ZE_TOOLBOX_ROOT_PATH/bin && \
  for fname in $(find $ZE_TOOLBOX_BIN_PATH -name '*.sh' -type f); do \
    chmod a+x $fname; \
    ln -s $fname /usr/bin/$(basename "$fname" .sh); \
  done

# internal version
ARG VERSION
RUN echo $VERSION > $ZE_TOOLBOX_ROOT_PATH/toolbox-version \
    && cd $ZE_TOOLBOX_NODE_PATH \
    && npm version --allow-same-version --no-git-tag-version $VERSION

CMD ["bash"]
